# ML3R_Caldas | Model Learning Rainfall for Caldas

These scripts contain the necessary algorithms to generate the ML3R model for the department of Caldas, Colombia. The language used to build the model was Python. For data processing algorithms and functions used libraries Numpy and SciPy.

The data processing is performed in parallel through aggregation MapReduce operation on a multi-node cluster with HDFS distributed file system supported Apache Hadoop technology.

About author: [mfreyeso][1]

[1]: https://twitter.com/mfreyeso